﻿using Models;
using Services;
using Services.Odsluchane;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xabe.FFmpeg;
using YoutubeDownloader;

namespace RadioScraper
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		ObservableCollection<SongModel> songs;

		public MainWindow()
		{
			InitializeComponent();

			songs = new ObservableCollection<SongModel>();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
            
            OdsluchaneService s = new OdsluchaneService();
            var list = s.GetSongFromDay(new DateTime(2018, 1, 3));

			if (list == null)
				return;
			
			list = list.Distinct().ToList();



     

            list.ForEach(x => songs.Add(x));

            songList.ItemsSource = songs;

    }

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			Thread thread = new Thread(DownloadPart);

			thread.Start();

		}

		private async void DownloadPart()
		{
			FFbase.FFmpegDir = @"C:\FFMPEG\bin";
			foreach (var song in songs)
			{
				try
				{
					YouDownloader downloader = new YouDownloader();
					await downloader.SaveAsync(song.URL, song.Author, song.Title);
				}
				catch(Exception e)
				{

				}
			}
		}
	}
}
