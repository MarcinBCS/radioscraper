﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeDownloader
{
	public class YouSearch
	{
		public string Search(string author, string title)
		{
			var youTubeApiRestClient = new YouTubeApiRestClient.YouTubeApiRestClient(apiKey: "AIzaSyDixQdZnKnzWv3v3Ko2UCwVqRHxwWice_k");

			string part = "snippet";
			string q = author + " " + title;
			long? maxResults = 50;
	

			var request = new YouTubeApiRestClient.Views.SearchResource.ListRequest(part);
			request.Order = YouTubeApiRestClient.Views.SearchResource.ListRequest.OrderEnum.ViewCount;
			request.Q = q;
			request.MaxResults = maxResults;
	
			var response = youTubeApiRestClient.Search(request);

			return response.Items.First().Id.VideoId;
		}

	}
}
