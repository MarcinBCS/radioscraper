﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xabe.FFmpeg;
using YoutubeExplode;


namespace YoutubeDownloader
{
	public class YouDownloader
	{

		public async Task SaveAsync(string id, string author, string title)
		{
		

			//var url = "https://www.youtube.com/watch?v=n2hJA78YuWw";
			//var id = YoutubeClient.ParseVideoId(url); // "bnsUkE8i0tU"

			var client = new YoutubeClient();
			var info = await client.GetVideoMediaStreamInfosAsync(id);
			var audioStream = info?.Audio?.OrderBy(s => s.Bitrate)?.Last();
			using (var input = await client.GetMediaStreamAsync(audioStream))
			using (var output = File.Create("D:/Downloads/"+author+"-"+title+".temp.webm"))
				await input.CopyToAsync(output);


			bool result = await ConversionHelper.ExtractAudio("D:/Downloads/" + author + "-" + title + ".temp.webm", "D:/Downloads/" + author + "-" + title + ".mp3")
														.Start();

			File.Delete("D:/Downloads/" + author + "-" + title + ".temp.webm");
		}
	}
}
