﻿using NScrape;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Youtube
{
	public class YoutubeScraper : Scraper
	{
		public YoutubeScraper(string html) : base(html)
		{
		}

		public string GetVideosIds()
		{
			var list = new List<YoutubeSong>();

			var videoParts = HtmlDocument.DocumentNode.SelectNodes("./ytd-video-renderer");

			foreach (var row in videoParts)
			{
				var links = row.Descendants();

				if (links != null)
				{
					var ids = links.Where((x) => x.Attributes.Contains("class") && x.Attributes["class"].Value.Equals("yt-simple-endpoint inline-block style-scope ytd-thumbnail")).FirstOrDefault().Attributes["href"].Value;
					var views = links.Where((x) => x.Attributes.Contains("class") && x.Attributes["class"].Value.Equals("style-scope ytd-video-meta-block")).FirstOrDefault().InnerText;
					
					list.Add(new YoutubeSong(ids,views));
				}
			}

			return list.OrderByDescending(x => x.Views).First().Ids;
		}
	}

	class YoutubeSong
	{
		public string Ids { get; set; }
		public int Views { get; set; }

		public YoutubeSong(string ids, string views)
		{
			ids = ids.Replace("/watch?v=", "");

			views = views.Replace("views", "");

			if (views.Contains("K"))
			{
				Views = int.Parse(views.Replace("K", "")) * 1000;
			}
			else
			{
				if (views.Contains("M"))
					Views = int.Parse(views.Replace("K", "")) * 1000000;
				else
					Views = int.Parse(views);
			}
		}
	}
}
