﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Services
{
    public abstract class BaseService : IService
    {
        public abstract List<SongModel> GetSongFromDay(DateTime date);        
        public abstract List<SongModel> GetSongFromMonth(DateTime date);

        protected ISongScraper SongScraper;

        public static string GetHtml(string urlAddress)
        {
            //string urlAddress = "https://www.odsluchane.eu/szukaj.php?r=8&date=04-01-2018&time_from=14&time_to=16";
           
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();

                return data;
            }

            return string.Empty;
        }
    }
}
