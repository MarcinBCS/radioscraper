﻿using Models;
using NScrape;
using Services.Youtube;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YoutubeDownloader;

namespace Services
{
	public class OdsluchaneSongScraper : Scraper, ISongScraper
	{
		public OdsluchaneSongScraper(string html) : base(html)
		{
		}

		public List<SongModel> GetSongs()
		{
			var list = new List<SongModel>();

			var tbody = HtmlDocument.DocumentNode.SelectNodes("//table/tbody/tr");

			foreach(var row in tbody)
			{
				var links = row.SelectNodes(".//a");

				try
				{


					if (links != null)
					{
						var titleAndAuthor = links.Where((x) => x.Attributes.Contains("class") && x.Attributes["class"].Value.Equals("title-link")).FirstOrDefault().InnerText.Trim();
						var url = links.Where((x) => x.Attributes.Contains("title") && x.Attributes["title"].Value.Equals("Posłuchaj w serwisie YouTube")).FirstOrDefault().Attributes["href"].Value.ToString().Trim();

						var song = new SongModel();
						song.Author = titleAndAuthor.Split('-')[0].Trim();
						song.Title = titleAndAuthor.Split('-')[1].Trim();

						YouSearch search = new YouSearch();
						song.URL = search.Search(song.Author, song.Title);
						
						list.Add(song);
					}
				}
				catch (Exception e)
				{
					Console.Write(e);
				}
			}

			return list;
		}
	}
}
