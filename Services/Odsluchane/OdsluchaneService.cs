﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace Services.Odsluchane
{
    public class OdsluchaneService : BaseService
    {
        private string urlPattern;

        public OdsluchaneService()
        {
            urlPattern = "https://www.odsluchane.eu/szukaj.php?r={0}&date={1}&time_from={2}&time_to={3}";
        }

        public override List<SongModel> GetSongFromDay(DateTime date)
        {
            var song = new List<SongModel>();

            for (int i = 0; i < 2; i++)
            {
                try
                {
                    OdsluchaneSongScraper scraper = new OdsluchaneSongScraper(GetHtml(string.Format(urlPattern, 8, date.ToString("dd-MM-yyyy"), 2 * i, 2 * i + 2)));
                    song.AddRange(scraper.GetSongs());
                }
                catch(Exception e)
                {
                    //dorobic logowanie
                    Console.WriteLine("Error on "+i*2+" on date" + date.ToShortDateString());
                    Console.WriteLine(e);
                }
            }

            return song;
        }

        public override List<SongModel> GetSongFromMonth(DateTime date)
        {
            throw new NotImplementedException();
        }
    }
}
